<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include_once ('include/trn.class.php');

include_once ('include/function2.inc.php');

if (isset($_POST['pratymas']) && isset($_POST['numb']))
	{
	$name = $_POST['pratymas'];
	$dvik = $_POST['numb'];
	if ($name == 'Dvikovė')
		{
		$dvik = $dvik * 1;
		}
	  else
	if ($name == 'Rovimas')
		{
		$dvik = ($dvik * 100) / 44.4;
		}
	  else
	if ($name == 'Stumimas')
		{
		$dvik = ($dvik * 100) / 55.6;
		}
	  else
	if ($name == 'Jėgos rovimas')
		{
		$dvik = ($dvik * 100) / 36.4;
		}
	  else
	if ($name == 'Jėgos stumimas')
		{
		$dvik = ($dvik * 100) / 45.6;
		}
	  else
	if ($name == 'Ant krutinės')
		{
		$dvik = ($dvik * 100) / 52.5;
		}
	  else
	if ($name == 'Pritupimai ant krutinės')
		{
		$dvik = ($dvik * 100) / 62;
		}
	  else
	if ($name == 'Pritupimai ant nugaros')
		{
		$dvik = ($dvik * 100) / 72;
		}
	  else
	if ($name == 'Trikovės trauka')
		{
		$dvik = ($dvik * 100) / 65;
		}
	  else
	if ($name == 'Spaudimas gulint')
		{
		$dvik = ($dvik * 100) / 40;
		}
	}
  else
	{
	$dvik = 0;
	}

$errors = array();

if (isset($_POST['generate']) && trim($_POST['generate']) != '')
	{
	$generate = true;
	}
  else
	{
	$generate = false;
	}

if (isset($_POST['snatch']) && trim($_POST['snatch']) != '')
	{
	$snatch = floatval($_POST['snatch']);
	if ($snatch == 0 || $snatch > 300)
		{
		unset($snatch);
		$errors[] = 'Blogai įvestas rovimas';
		}
	}
  else
	{
	if ($generate) $errors[] = 'Neįvestas rovimas';
	}

if (isset($_POST['oddeven']))
	{
	$oddeven = true;
	$oddeven_text = 1;
	}
  else
	{
	$oddeven = false;
	$oddeven_text = 0;
	}

if (isset($_POST['type_calculate']) && !empty($_POST['type_calculate']))
	{
	foreach($_POST['type_calculate'] as $key => $value)
		{
		if (isset($value) && !empty($value)) $type_calculate = trim($value);
		}
	}
  else
	{
	$type_calculate = 'linear';

	// $errors[]='Nepasirinktas režimas';

	}

if (isset($_POST['BirthYear']) && isset($_POST['BirthMonth']) && isset($_POST['BirthDay']))
	{
	$year = intval($_POST['BirthYear']);
	$month = intval($_POST['BirthMonth']);
	$day = intval($_POST['BirthDay']);
	if (checkdate($month, $day, $year))
		{
		$inp = '<select name="BirthYear" id="BirthYear">';
		for ($i = date('Y'); $i > 1940; $i--)
			{
			if ($i == $year)
				{
				$checked = 'selected';
				}
			  else
				{
				$checked = '';
				}

			$inp.= '<option value="' . $i . '" ' . $checked . '>' . $i . '</option>';
			}

		$months = array(
			'sausis',
			'vasaris',
			'kovas',
			'balandis',
			'gegužė',
			'birželis',
			'liepa',
			'rugpjūtis',
			'rugsėjis',
			'spalis',
			'lapkritis',
			'gruodis'
		);
		$inp.= '</select> <select name="BirthMonth" id="BirthMonth">';
		for ($i = 0; $i < count($months); $i++)
			{
			if (($i + 1) == $month)
				{
				$checked = 'selected';
				}
			  else
				{
				$checked = '';
				}

			$inp.= '<option value="' . ($i + 1) . '" ' . $checked . '>' . $months[$i] . '</option>';
			}

		$inp.= '</select> <select name="BirthDay" id="BirthDay">';
		for ($i = 1; $i < 32; $i++)
			{
			if ($i == $day)
				{
				$checked = 'selected';
				}
			  else
				{
				$checked = '';
				}

			$inp.= '<option value="' . $i . '" ' . $checked . '>' . $i . '</option>';
			}

		$inp.= '</select> ';
		$lifedays = abs(gregorianToJD2($month, $day, $year) - gregorianToJD2(date("m") , date("d") , date("Y")));
		$birthdate = array(
			'year' => $year,
			'month' => $month,
			'day' => $day,
			'days' => $lifedays,
			'timebirth' => strtotime($year . '-' . $month . '-' . $day) ,
			'select' => $inp,
			'title' => 'Gimimo data'
		);
		}
	  else
		{
		$errors[] = 'Blogai įvesta gimimo data';
		$inp = '<select name="BirthYear" id="BirthYear">';
		for ($i = date('Y'); $i > 1940; $i--)
			{
			$inp.= '<option value="' . $i . '">' . $i . '</option>';
			}

		$months = array(
			'sausis',
			'vasaris',
			'kovas',
			'balandis',
			'gegužė',
			'birželis',
			'liepa',
			'rugpjūtis',
			'rugsėjis',
			'spalis',
			'lapkritis',
			'gruodis'
		);
		$inp.= '</select> <select name="BirthMonth" id="BirthMonth">';
		for ($i = 0; $i < count($months); $i++)
			{
			$inp.= '<option value="' . ($i + 1) . '">' . $months[$i] . '</option>';
			}

		$inp.= '</select> <select name="BirthDay" id="BirthDay">';
		for ($i = 1; $i < 32; $i++)
			{
			$inp.= '<option value="' . $i . '" >' . $i . '</option>';
			}

		$inp.= '</select> ';
		$birthdate = array(
			'select' => $inp,
			'title' => 'Gimimo data',
			'timebirth' => ''
		);
		}
	}

if (isset($_POST['CompYear']) && isset($_POST['CompMonth']) && isset($_POST['CompDay']))
	{
	$year = intval($_POST['CompYear']);
	$month = intval($_POST['CompMonth']);
	$day = intval($_POST['CompDay']);
	if (checkdate($month, $day, $year))
		{
		if (time() < strtotime($year . '-' . $month . '-' . $day))
			{
			$inp = '<select name="CompYear" id="CompYear">';
			for ($i = date('Y'); $i < (date('Y') + 2); $i++)
				{
				if ($i == $year)
					{
					$checked = 'selected';
					}
				  else
					{
					$checked = '';
					}

				$inp.= '<option value="' . $i . '" ' . $checked . '>' . $i . '</option>';
				}

			$months = array(
				'sausis',
				'vasaris',
				'kovas',
				'balandis',
				'gegužė',
				'birželis',
				'liepa',
				'rugpjūtis',
				'rugsėjis',
				'spalis',
				'lapkritis',
				'gruodis'
			);
			$inp.= '</select> <select name="CompMonth" id="CompMonth">';
			if ($year == date('Y'))
				{
				$i = date('m') - 1;
				}
			  else
				{
				$i = 0;
				}

			for ($i; $i < count($months); $i++)
				{
				if (($i + 1) == $month)
					{
					$checked = 'selected';
					}
				  else
					{
					$checked = '';
					}

				$inp.= '<option value="' . ($i + 1) . '" ' . $checked . '>' . $months[$i] . '</option>';
				}

			$inp.= '</select> <select name="CompDay" id="CompDay">';
			if ($year == date('Y') && $month == date('m'))
				{
				$i = date('d');
				}
			  else
				{
				$i = 1;
				}

			for ($i; $i < 32; $i++)
				{
				if ($i == $day)
					{
					$checked = 'selected';
					}
				  else
					{
					$checked = '';
					}

				$inp.= '<option value="' . $i . '" ' . $checked . '>' . $i . '</option>';
				}

			$inp.= '</select> ';
			$datefrom = time();
			$dateto = strtotime($year . '-' . $month . '-' . $day, 0);
			$difference = $dateto - $datefrom;
			$datediff = intval(floor($difference / 604800));
			$compdate = array(
				'year' => $year,
				'month' => $month,
				'day' => $day,
				'select' => $inp,
				'timecomp' => strtotime($year . '-' . $month . '-' . $day) ,
				'title' => 'Varžybų data',
				'weeks' => $datediff
			);
			}
		  else
			{
			$errors[] = 'Blogai įvesta varžybų  data( negali būti praeityje)';
			$inp = '<select name="CompYear" id="CompYear">';
			for ($i = date('Y'); $i < (date('Y') + 2); $i++)
				{
				$inp.= '<option value="' . $i . '">' . $i . '</option>';
				}

			$months = array(
				'sausis',
				'vasaris',
				'kovas',
				'balandis',
				'gegužė',
				'birželis',
				'liepa',
				'rugpjūtis',
				'rugsėjis',
				'spalis',
				'lapkritis',
				'gruodis'
			);
			$inp.= '</select> <select name="CompMonth" id="CompMonth">';
			for ($i = date('m') - 1; $i < count($months); $i++)
				{
				$inp.= '<option value="' . ($i + 1) . '">' . $months[$i] . '</option>';
				}

			$inp.= '</select> <select name="CompDay" id="CompDay">';
			for ($i = date('d'); $i < 32; $i++)
				{
				$inp.= '<option value="' . $i . '" >' . $i . '</option>';
				}

			$inp.= '</select> ';
			$compdate = array(
				'select' => $inp,
				'title' => 'Varžybų data',
				'timecomp' => ''
			);
			}
		}
	  else
		{
		$errors[] = 'Blogai įvesta varžybų  data';
		$inp = '<select name="CompYear" id="CompYear">';
		for ($i = date('Y'); $i < (date('Y') + 2); $i++)
			{
			$inp.= '<option value="' . $i . '">' . $i . '</option>';
			}

		$months = array(
			'sausis',
			'vasaris',
			'kovas',
			'balandis',
			'gegužė',
			'birželis',
			'liepa',
			'rugpjūtis',
			'rugsėjis',
			'spalis',
			'lapkritis',
			'gruodis'
		);
		$inp.= '</select> <select name="CompMonth" id="CompMonth">';
		for ($i = date('m') - 1; $i < count($months); $i++)
			{
			$inp.= '<option value="' . ($i + 1) . '">' . $months[$i] . '</option>';
			}

		$inp.= '</select> <select name="CompDay" id="CompDay">';
		for ($i = date('d'); $i < 32; $i++)
			{
			$inp.= '<option value="' . $i . '" >' . $i . '</option>';
			}

		$inp.= '</select> ';
		$compdate = array(
			'select' => $inp,
			'title' => 'Varžybų data'
		);
		}
	}

if (isset($_POST['cleanjerk']) && trim($_POST['cleanjerk']) != '')
	{
	$cleanjerk = floatval($_POST['cleanjerk']);
	if ($cleanjerk == 0 || $cleanjerk > 300)
		{
		unset($cleanjerk);
		$errors[] = 'Blogai įvestas stumimas';
		}
	}
  else
	{
	if ($generate) $errors[] = 'Neįvestas stumimas';
	}

if (isset($snatch) && isset($cleanjerk))
	{
	$dvik = $snatch + $cleanjerk;
	}

if (isset($_POST['squats_front']) && trim($_POST['squats_front']) != '')
	{
	$squats_front = floatval($_POST['squats_front']);
	if ($squats_front == 0 || $squats_front > 300)
		{
		unset($squats_front);
		$errors[] = 'Blogai įvesti pritupimai štanga ant krutinės';
		}
	}
  else
	{
	if ($generate) $errors[] = 'Neįvesti pritupimai štanga ant krutinės';
	}

if (isset($_POST['squats_back']) && trim($_POST['squats_back']) != '')
	{
	$squats_back = floatval($_POST['squats_back']);
	if ($squats_back == 0 || $squats_back > 300)
		{
		unset($squats_back);
		$errors[] = 'Blogai įvesti pritupimai štanga ant nugaros';
		}
	}
  else
	{
	if ($generate) $errors[] = 'Neįvesti pritupimai štanga ant nugaros';
	}

if (isset($_POST['standfront']) && trim($_POST['standfront']) != '')
	{
	$standfront = floatval($_POST['standfront']);
	if ($standfront == 0 || $standfront > 300)
		{
		unset($standfront);
		$errors[] = 'Blogai įvestas stumimas nuo krutines';
		}
	}
  else
	{
	if ($generate) $errors[] = 'Neįvestas stumimas nuo krutines';
	}

if (isset($_POST['standbehind']) && trim($_POST['standbehind']) != '')
	{
	$standbehind = floatval($_POST['standbehind']);
	if ($standbehind == 0 || $standbehind > 300)
		{
		unset($standbehind);
		$errors[] = 'Blogai įvestas stumimas nuo nugaros';
		}
	}
  else
	{
	if ($generate) $errors[] = 'Neįvestas stumimas nuo nugaros';
	}

if (isset($_POST['weeks']) && trim($_POST['weeks']) != '')
	{
	$weeks = intval($_POST['weeks']);
	if ($weeks == 0 || $weeks > 56)
		{
		unset($weeks);
		$errors[] = 'Blogai įvestas savaičių kiekis';
		}
	}
elseif (!isset($_POST['weeks']) && isset($compdate))
	{
	if (isset($compdate['weeks']))
		{
		if ($compdate['weeks'] > 0)
			{
			$weeks = $compdate['weeks'];
			}
		  else
			{
			$errors[] = 'Liko paskutinė savaitė iki varžybų';
			}
		}
	  else
		{
		$errors[] = 'Pasirinkta netaisklynga varžybu data';
		}
	}
  else
	{
	if ($generate) $errors[] = 'Neįvestas savaičių kiekis';
	}

if (isset($_POST['tranings']) && trim($_POST['tranings']) != '')
	{
	$tranings = intval($_POST['tranings']);
	if ($tranings == 0 || $tranings > 7)
		{
		unset($tranings);
		$errors[] = 'Blogai įvestas treniruočių kiekis';
		}
	}
  else
	{
	if ($generate) $errors[] = 'Neįvestas treniruočių kiekis';
	}

if (isset($snatch) && isset($cleanjerk) && isset($dvik) && isset($standfront) && isset($standbehind) && isset($squats_front) && isset($squats_back) && isset($weeks) && isset($tranings))
	{
	}
  else
	{
	if (!empty($errors))
		{
		$table = '<div class="error">';
		foreach($errors as $key => $value)
			{
			$table.= ' <span>' . $value . '</span> <br/>';
			}

		$table.= ' </div> ';
		}
	}

$form = '<div class="error"><span>Dėmesio ! Naudoti šią treniruočių generavimo programėlę be trenerio priežiūros yra pavojinga savo ir aplinkiniu sveikatai !  Interneto puslapio kūrėjai neatsako už šios treniruočių programos naudojimo pasekmės.</span><br/>
              Generatorius dabar yra &beta;-testavimo būsenoje. Jei Jus aptiksite netikslumus arba klaidas praneškite el. paštu - <img src="email2.php" alt="El. paštas" style="border:none;float:right;"><br class="clear"></div>
<form name="skaic3" method="post" action="trn.php" class="frm">
<div class="leftformtable">
<table >
<tr >
    <td colspan="3">Įveskite savo maksilamlius rezultatus:</td>
    </tr >
	<tr >
	<td>Rovimas</td>
    <td colspan="2"><input type="text" name="snatch" size="5" maxlength="5" value="';

if (isset($_POST['snatch']) && trim($_POST['snatch']) != '' && isset($snatch) && $snatch != 0)
	{
	$form.= round($snatch, 1);
	}

$form.= '">kg</td>
    </tr >
    <tr >
    <td>Stumimas</td>
    <td colspan="2"><input type="text" name="cleanjerk" size="5" maxlength="5" value="';

if (isset($_POST['cleanjerk']) && trim($_POST['cleanjerk']) != '' && isset($cleanjerk) && $cleanjerk != 0)
	{
	$form.= round($cleanjerk, 1);
	}

$form.= '">kg</td>
    </tr >
    <tr >
    <td>Pritupimai su štanga ant krutinės</td>
    <td colspan="2"><input type="text" name="squats_front" size="5" maxlength="5" value="';

if (isset($_POST['squats_front']) && trim($_POST['squats_front']) != '' && isset($squats_front) && $squats_front != 0)
	{
	$form.= round($squats_front, 1);
	}

$form.= '">kg</td>
    </tr >
    <tr >
    <td>Pritupimai su štanga ant nugaros</td>
    <td colspan="2"><input type="text" name="squats_back" size="5" maxlength="5" value="';

if (isset($_POST['squats_back']) && trim($_POST['squats_back']) != '' && isset($squats_back) && $squats_back != 0)
	{
	$form.= round($squats_back, 1);
	}

$form.= '">kg</td>
    </tr >
    <tr >
    <td>Štangos stumimas nuo krutinės (nuo stovu)</td>
    <td colspan="2"><input type="text" name="standfront" size="5" maxlength="5" value="';

if (isset($_POST['standfront']) && trim($_POST['standfront']) != '' && isset($standfront) && $standfront != 0)
	{
	$form.= round($standfront, 1);
	}

$form.= '">kg</td>
    </tr >
    <tr >
    <td>Štangos stumimas nuo nugaros (nuo stovu)</td>
    <td colspan="2"><input type="text" name="standbehind" size="5" maxlength="5" value="';

if (isset($_POST['standbehind']) && trim($_POST['standbehind']) != '' && isset($standbehind) && $standbehind != 0)
	{
	$form.= round($standbehind, 1);
	}

$form.= '">kg</td>
    </tr >
    <tr >
    <td>Savaitės</td>
    <td colspan="2"><input type="text" name="weeks" size="1" maxlength="1" value="';

if (isset($weeks) && $weeks != 0)
	{
	$form.= $weeks;
	}
elseif (isset($compdate) && isset($compdate['weeks']))
	{
	$form.= $compdate['weeks'];
	}

$form.= '"';

if (isset($compdate))
	{
	$form.= 'disabled';
	}

$form.= '></td>
    </tr >
    <tr >
    <td>Treniruotės</td>
    <td colspan="2"><input type="text" name="tranings" size="1" maxlength="1" value="';

if (isset($_POST['tranings']) && trim($_POST['tranings']) != '' && isset($tranings) && $tranings != 0)
	{
	$form.= $tranings;
	}

$form.= '">kartu per savaitė</td>
    </tr >


</table>
</div>
<div class="rightformtable">
<table>
<tr style="border:thin solid black;margin:0;padding:0;"><td>Režimas</td><td><input type="radio" name="type_calculate[]" value="linear" ';

if (!isset($type_calculate) || (isset($type_calculate) && strcmp($type_calculate, 'linear') === 0)) $form.= 'checked';
$form.= '> Augantis <br/><input type="radio" name="type_calculate[]" value="competition" ';

if (isset($type_calculate) && strcmp($type_calculate, 'competition') === 0) $form.= 'checked';
$form.= '> Varžybinis <br/><input type="radio" name="type_calculate[]" value="biorythm" ';

if (isset($type_calculate) && strcmp($type_calculate, 'biorythm') === 0) $form.= 'checked';
$form.= '> Bioritminis <br/><input type="radio" name="type_calculate[]" value="static" ';

if (isset($type_calculate) && strcmp($type_calculate, 'static') === 0) $form.= 'checked';
$form.= '> Pastovus <br/></td></tr>
<tr style="border:thin solid black;margin:5px 0;padding:0;"><td>Pakesti vietomis rovimą ir stumimą (jei atžymėtą tai nelyginėmis treneruotenis bus stumimimai)</td><td><input type="checkbox" name="oddeven" ';

if (isset($oddeven) && $oddeven)
	{
	$form.= 'checked';
	}

$form.= '/></td></tr>
<tr><td><div id="moretitleform">';

if (isset($birthdate) && $birthdate['title'] != '')
	{
	$form.= $birthdate['title'];
	}

if (isset($compdate) && $compdate['title'] != '')
	{
	$form.= $compdate['title'];
	}

$form.= '</div></td><td><div id="moreblockform">';

if (isset($birthdate) && $birthdate['select'] != '')
	{
	$form.= $birthdate['select'];
	}

if (isset($compdate) && $compdate['select'] != '')
	{
	$form.= $compdate['select'];
	}

$form.= '</div></td></tr>

</table>
</div>
<br class="clear"/>
<table style="width:100%;">
<tr >
    <td><input type="submit" value="Generuoti" name="generate" value="go"></td>
</tr>

</table>
</form>';
?>
<div class="Box">
  <div class="BoxHeader">
      Treniruotės grafiko sudarymas <?php

if (isset($mobile) && is_array($mobile) && $mobile[1])
	{
	echo 'mobili versija';
	}
  else
	{
	echo 'pilna versija';
	} ?>
  </div>
<div class="BoxBody">
<?php

if (isset($mobile) && is_array($mobile) && !$mobile[1]) $browsers = getBrowser();

// var_dump($browsers);

if (isset($browsers))
	{
	if ((strcmp($browsers['name'], 'Internet Explorer') === 0 && (double)$browsers['version'] > 9.0) || (strcmp($browsers['name'], 'Internet Explorer') !== 0))
		{
		echo $form;
		}
	  else
		{
		echo 'Patariama naudoti naujesnie negu Internet Explorer naršyklę (versija turi būti didesne negu 9).<br/>';
		}
	}
  else
	{
	echo $form;
	}

?>
		<?php

if (empty($errors) && isset($snatch) && isset($cleanjerk) && isset($dvik) && isset($standfront) && isset($standbehind) && isset($squats_front) && isset($squats_back) && isset($weeks) && isset($tranings))
	{

	// echo '<div id="contentblock"></div><div id="helpblock"></div>';

	if (isset($mobile) && is_array($mobile) && $mobile[1])
		{
		include_once ('include/ShowMobileTrainsData.php');

		// $webpage='include/mobileTrainsData.php';

		}
	  else
		{
		include_once ('include/ShowTrainsData.php');

		// $webpage='include/TrainsData.php';

		}
	}
  else
	{
	if (isset($table) && $table != '') echo $table;
	}

?>
               </div>
   </div>  