

<?php
$answer = array();
$answer['answer'] = false;

if (isset($snatch) && isset($cleanjerk) && isset($dvik) && isset($standfront) && isset($standbehind) && isset($squats_front) && isset($squats_back) && isset($weeks) && isset($tranings))
	{
	$mas = array();
	if (class_exists('Trainer'))
		{
		$mas['snatch'] = new Trainer(1, 'Rovimas klasika', 'snatch.png', $snatch, 1, $weeks, $tranings, true);
		$mas['snatch_high'] = new Trainer(2, 'Rovimas nuo pakibimo', 'snatch_high.png', $snatch, 1, $weeks, $tranings, true);
		$mas['snatch_strenght'] = new Trainer(3, 'Trauka rovimo', 'snatch_strenght.png', $snatch, 2, $weeks, $tranings, false);
		$mas['cleanjerk'] = new Trainer(4, 'Stumimas klasika', 'cleanjerk.png', $cleanjerk, 2, $weeks, $tranings, true);
		$mas['cleanjerk_high'] = new Trainer(5, 'Stumimas nuo pakibimo', 'cleanjerk_high.png', $cleanjerk, 2, $weeks, $tranings, true);
		$mas['clean'] = new Trainer(6, 'Štangos paėmimas ant krutinės', 'clean.png', $cleanjerk, 2, $weeks, $tranings, true);
		$mas['cleanjerk_strenght'] = new Trainer(7, 'Trauka stumimo', 'cleanjerk_strenght.png', $cleanjerk, 1, $weeks, $tranings, false);
		$mas['standfront'] = new Trainer(8, 'Štangos stumimas nuo krutinės (nuo stovu)', 'standfront.png', $standfront, 1, $weeks, $tranings, false);
		$mas['standbehind'] = new Trainer(9, 'Štangos stumimas nuo nugaros (nuo stovu)', 'standbehind.png', $standbehind, 2, $weeks, $tranings, false);
		$mas['squats_front'] = new Trainer(10, 'Pritupimai štanga ant krutinės', 'squats_front.png', $squats_front, 2, $weeks, $tranings, false);
		$mas['squats_back'] = new Trainer(11, 'Pritupimai štanga ant nugaros', 'squats_back.png', $squats_back, 1, $weeks, $tranings, false);
		}

	$answer['answer'] = true;

	// <img src="images/pdf.png" width="24" height="24" border="0" id="generatePDF" alt="weeks='.$weeks.'&tranings='.$tranings.'&squats_back='.$squats_back.'&squats_front='.$squats_front.'&standbehind='.$standbehind.'&standfront='.$standfront.'&cleanjerk='.$cleanjerk.'&snatch='.$snatch.'&dvik='.$dvik.'"> - atsiųsti treneruočiu lentelė PDF bylą<br/>
	// <img src="images/pdf.png" width="24" height="24" border="0" id="generatePDFList" alt="weeks='.$weeks.'&tranings='.$tranings.'&squats_back='.$squats_back.'&squats_front='.$squats_front.'&standbehind='.$standbehind.'&standfront='.$standfront.'&cleanjerk='.$cleanjerk.'&snatch='.$snatch.'&dvik='.$dvik.'"> - atsiųsti treneruočiu sąrašą PDF bylą<br/>

	$table = '<a href="#documentation">Sugeneruotos treneruočių programos paaiškinimas</a>  <br/>
		
';
	switch ($type_calculate)
		{
	case 'linear':
		$table.= ' 
												<img src="images/doc.png" width="24" height="24" border="0" id="generatePrint" alt="weeks=' . $weeks . '&tranings=' . $tranings . '&squats_back=' . $squats_back . '&squats_front=' . $squats_front . '&standbehind=' . $standbehind . '&standfront=' . $standfront . '&cleanjerk=' . $cleanjerk . '&snatch=' . $snatch . '&dvik=' . $dvik . '&typecalculate=1&oddeven=' . $oddeven_text . '"> - lentelės spausdinimo versija (atsidaro naujam lange, po atsidarymo spaudžiame klavišų kombinaciją CTRL + P, kurį iškviečia spausdintuvo pasirinkimą)<br/>
												<img src="images/doc.png" width="24" height="24" border="0" id="generatePrintList" alt="weeks=' . $weeks . '&tranings=' . $tranings . '&squats_back=' . $squats_back . '&squats_front=' . $squats_front . '&standbehind=' . $standbehind . '&standfront=' . $standfront . '&cleanjerk=' . $cleanjerk . '&snatch=' . $snatch . '&dvik=' . $dvik . '&typecalculate=1&oddeven=' . $oddeven_text . '"> - sąrašo spausdinimo versija (atsidaro naujam lange, po atsidarymo spaudžiame klavišų kombinaciją CTRL + P, kurį iškviečia spausdintuvo pasirinkimą)<br/> ';
		break;

	case 'competition':
		$table.= ' 
												<img src="images/doc.png" width="24" height="24" border="0" id="generatePrint" alt="weeks=' . $weeks . '&tranings=' . $tranings . '&squats_back=' . $squats_back . '&squats_front=' . $squats_front . '&standbehind=' . $standbehind . '&standfront=' . $standfront . '&cleanjerk=' . $cleanjerk . '&snatch=' . $snatch . '&dvik=' . $dvik . '&typecalculate=2&oddeven=' . $oddeven_text . '&compdate=' . $compdate['timecomp'] . '"> - lentelės spausdinimo versija (atsidaro naujam lange, po atsidarymo spaudžiame klavišų kombinaciją CTRL + P, kurį iškviečia spausdintuvo pasirinkimą)<br/>
												<img src="images/doc.png" width="24" height="24" border="0" id="generatePrintList" alt="weeks=' . $weeks . '&tranings=' . $tranings . '&squats_back=' . $squats_back . '&squats_front=' . $squats_front . '&standbehind=' . $standbehind . '&standfront=' . $standfront . '&cleanjerk=' . $cleanjerk . '&snatch=' . $snatch . '&dvik=' . $dvik . '&typecalculate=2&oddeven=' . $oddeven_text . '&compdate=' . $compdate['timecomp'] . '"> - sąrašo spausdinimo versija (atsidaro naujam lange, po atsidarymo spaudžiame klavišų kombinaciją CTRL + P, kurį iškviečia spausdintuvo pasirinkimą)<br/> ';
		break;

	case 'biorythm':
		$table.= ' 
												<img src="images/doc.png" width="24" height="24" border="0" id="generatePrint" alt="weeks=' . $weeks . '&tranings=' . $tranings . '&squats_back=' . $squats_back . '&squats_front=' . $squats_front . '&standbehind=' . $standbehind . '&standfront=' . $standfront . '&cleanjerk=' . $cleanjerk . '&snatch=' . $snatch . '&dvik=' . $dvik . '&typecalculate=3&oddeven=' . $oddeven_text . '&birthdate=' . $birthdate['timebirth'] . '"> - lentelės spausdinimo versija (atsidaro naujam lange, po atsidarymo spaudžiame klavišų kombinaciją CTRL + P, kurį iškviečia spausdintuvo pasirinkimą)<br/>
												<img src="images/doc.png" width="24" height="24" border="0" id="generatePrintList" alt="weeks=' . $weeks . '&tranings=' . $tranings . '&squats_back=' . $squats_back . '&squats_front=' . $squats_front . '&standbehind=' . $standbehind . '&standfront=' . $standfront . '&cleanjerk=' . $cleanjerk . '&snatch=' . $snatch . '&dvik=' . $dvik . '&typecalculate=3&oddeven=' . $oddeven_text . '&birthdate=' . $birthdate['timebirth'] . '"> - sąrašo spausdinimo versija (atsidaro naujam lange, po atsidarymo spaudžiame klavišų kombinaciją CTRL + P, kurį iškviečia spausdintuvo pasirinkimą)<br/> ';
		break;

	case 'static':
		$table.= ' 
												<img src="images/doc.png" width="24" height="24" border="0" id="generatePrint" alt="weeks=' . $weeks . '&tranings=' . $tranings . '&squats_back=' . $squats_back . '&squats_front=' . $squats_front . '&standbehind=' . $standbehind . '&standfront=' . $standfront . '&cleanjerk=' . $cleanjerk . '&snatch=' . $snatch . '&dvik=' . $dvik . '&typecalculate=4&oddeven=' . $oddeven_text . '"> - lentelės spausdinimo versija (atsidaro naujam lange, po atsidarymo spaudžiame klavišų kombinaciją CTRL + P, kurį iškviečia spausdintuvo pasirinkimą)<br/>
												<img src="images/doc.png" width="24" height="24" border="0" id="generatePrintList" alt="weeks=' . $weeks . '&tranings=' . $tranings . '&squats_back=' . $squats_back . '&squats_front=' . $squats_front . '&standbehind=' . $standbehind . '&standfront=' . $standfront . '&cleanjerk=' . $cleanjerk . '&snatch=' . $snatch . '&dvik=' . $dvik . '&typecalculate=4&oddeven=' . $oddeven_text . '"> - sąrašo spausdinimo versija (atsidaro naujam lange, po atsidarymo spaudžiame klavišų kombinaciją CTRL + P, kurį iškviečia spausdintuvo pasirinkimą)<br/> ';
		break;
		}

	$table.= '<table id="train_table" class="responsive" >
					<thead>
						<tr>
							<th class="trains textvertical">Savaitė</th>
							<th class="trains textvertical">Trenirotė</th> ';
	if (!empty($mas))
		{
		foreach($mas as $key => $value)
			{
			$table.= ' <th title="' . $value->name . '">' . $value->getName() . '</th> ';
			}
		}

	$table.= '</tr>
					</thead>
					<tbody>';
	$trn = 1;
	if (isset($oddeven) && $oddeven)
		{
		for ($i = 0; $i < $weeks; $i++)
			{
			$table.= '<tr><td class="trains" rowspan="' . ($tranings + 1) . '">' . ($i + 1) . ' </td>';

			// if(!empty($mas)){$table.='colspan="'.(count($mas)+1).'"';}else{$table.='colspan="2"';}

			$table.= '</tr>';
			for ($j = 0; $j < $tranings; $j++)
				{
				$table.= '<tr>
								<td class="trains"> ' . Roman_Number($j + 1) . ' <br/> ' . $trn . '</td>';
				if (!empty($mas))
					{
					foreach($mas as $key => $value)
						{
						switch ($type_calculate)
							{
						case 'linear':
							$table.= ' <td>' . $value->Calc2($i, $j, $tranings) . '</td> ';
							break;

						case 'competition':
							$table.= ' <td>' . $value->Calc4($i, $j, $tranings) . '</td> ';
							break;

						case 'biorythm':
							$table.= ' <td>' . $value->Calc6($i, $j, $tranings, $birthdate['days']) . '</td> ';
							break;

						case 'static':
							$table.= ' <td>' . $value->Calc8($i, $j, $tranings) . '</td> ';
							break;
							}
						}
					}

				$table.= '</tr>';
				$trn++;
				}

			$table.= '';
			}
		}
	  else
		{
		for ($i = 0; $i < $weeks; $i++)
			{
			$table.= '<tr><td class="trains" rowspan="' . ($tranings + 1) . '">' . ($i + 1) . ' </td>';

			// if(!empty($mas)){$table.='colspan="'.(count($mas)+1).'"';}else{$table.='colspan="2"';}

			$table.= '</tr>';
			for ($j = 0; $j < $tranings; $j++)
				{
				$table.= '<tr>
								<td class="trains"> ' . Roman_Number($j + 1) . ' <br/> ' . $trn . '</td>';
				if (!empty($mas))
					{
					foreach($mas as $key => $value)
						{

						// $table.=' <td>'.$value->Calc($i,$j,$tranings).'</td> ';

						switch ($type_calculate)
							{
						case 'linear':
							$table.= ' <td>' . $value->Calc($i, $j, $tranings) . '</td> ';
							break;

						case 'competition':
							$table.= ' <td>' . $value->Calc3($i, $j, $tranings) . '</td> ';
							break;

						case 'biorythm':
							$table.= ' <td>' . $value->Calc5($i, $j, $tranings, $birthdate['days']) . '</td> ';
							break;

						case 'static':
							$table.= ' <td>' . $value->Calc7($i, $j, $tranings) . '</td> ';
							break;
							}
						}
					}

				$table.= '</tr>';
				$trn++;
				}

			$table.= '';
			}
		}

	$table.= '</tbody></table>';
	$help = '';
	if (!empty($mas))
		{
		$help.= '<br/><div><a name="documentation">Aprašymas</a>:
Pirmame lentelės stulpelyje -  savaitės numeris.
Antrame  lentelės stulpelyje – treniruotes eilės numeris ir eile savaitėje.
Sekantis stulpeliai atvaizduoja pratimus :<br/>';
		$help.= '<div>';
		$i = 3;
		foreach($mas as $key => $value)
			{
			$help.= ' ' . $i . ' stulpelis  ' . $value->getName() . ' - ' . $value->GetLink() . ' <br/>';
			$i++;
			}

		$help.= '</div>';
		$help.= 'Treniruotės išdėstymas yra horizontalus, t. y. Nuo treniruotes eiles numerio link dešinės yra apskaičiuota kiek procentu arba kilogramų reikia pakelti, kiek padaryti priėjimų ir pakartojimų.</div>';
		}

	$answer['data'] = $table;
	$answer['help'] = $help;
	}

echo '<div id="contentblock">' . $answer['data'] . '</div><div id="helpblock">' . $answer['help'] . '</div>';
?>

