<?php
class Trainer

	{
	var $id;
	var $name;
	var $img;
	var $freq;
	var $mass;
	var $all;
	var $weeks;
	var $trainigs;
	var $const1;
	var $const2;
	var $const3;
	var $mas;
	var $mas1;
	var $mas2;
	var $mas3;
	var $procents1;
	var $procents2;
	var $procents3;
	var $PrilepinTable;
	var $planning1;
	var $planning2;
	var $planning3;
	var $planning4;
	public

	function random_int_float($min, $max)
		{
		return round(($min + lcg_value() * (abs($max - $min))));
		}

	public

	function calibrate($val)
		{
		$v = array(
			0,
			2.5,
			5,
			7.5
		);
		$v2 = array(
			0,
			2.5,
			5,
			7.5
		);
		$lastind = 1;
		$n = $val;
		while ($n > 0)
			{
			$r = $n / 10;
			$d = $n % 10;
			$x = floor($r) * 10;
			$y = abs($val - $x);

			// $v[]=$y;

			$n = $n - round($r * 10);
			}

		if ($y < 7.5)
			{
			$start = $v2[0] + $x;
			}
		  else
			{
			$start = $v2[count($v2) - 1] + $x;
			}

		$end = $v2[count($v2) - 1] + $x + 2.5;
		$mid = $end;
		$sum = $start;
		$t = true;
		while ($sum < $end)
			{
			if ($sum > $val && $t)
				{
				$t = false;
				$start = $sum - 2.5;
				$mid = $sum;
				$lastind++;
				}

			$sum+= 2.5;
			}

		if ($start <= $val && $val <= $mid)
			{
			$a = abs($val - $mid);
			$b = abs($val - $start);
			if ($a > $b)
				{
				$num = $start;
				}
			  else
				{
				$num = $mid;
				}
			}
		  else
			{
			if (!in_array($y, $v))
				{
				if ($y > 0 && $y < 2.5)
					{
					$num = floor($r) * 10;
					}
				elseif ($y > 2.5 && $y < 5)
					{
					$num = floor($r) * 10 + 2.5;
					}
				elseif ($y > 5 && $y < 7.5)
					{
					$num = floor($r) * 10 + 5;
					}
				elseif ($y > 7.5)
					{
					$num = round($r) * 10;
					}
				}
			  else
				{
				$num = $val;
				}
			}

		return $num;
		}

	public

	function GetName()
		{
		if (file_exists('images/' . $this->img))
			{
			$str = '<img src="images/' . $this->img . '" width="50" height="71" border="0" alt="' . $this->name . '"/>';
			}
		elseif (file_exists('../images/' . $this->img))
			{
			$str = '<img src="../images/' . $this->img . '" width="50" height="71" border="0" alt="' . $this->name . '"/>';
			}
		  else
			{
			$str = '' . mb_substr($this->name, 0, 5, 'UTF-8') . '...';
			}

		return '<a href="#des' . $this->id . '">' . $str . '</a>';
		}

	public

	function GetNameMid()
		{
		if (file_exists('images/' . $this->img))
			{
			$str = '<img src="images/' . $this->img . '" width="50" height="71" border="0" alt="' . $this->name . '"/>';
			}
		elseif (file_exists('../images/' . $this->img))
			{
			$str = '<img src="../images/' . $this->img . '" width="50" height="71" border="0" alt="' . $this->name . '"/>';
			}
		  else
			{
			$str = '' . mb_substr($this->name, 0, 5, 'UTF-8') . '...';
			}

		return '' . $str . '';
		}

	public

	function GetNameMin()
		{
		if (file_exists('images/' . $this->img))
			{
			$str = '<img src="images/' . $this->img . '" width="25" height="35" border="0" alt="' . $this->name . '"/>';
			}
		elseif (file_exists('../images/' . $this->img))
			{
			$str = '<img src="../images/' . $this->img . '" width="25" height="35" border="0" alt="' . $this->name . '"/>';
			}
		  else
			{
			$str = '' . mb_substr($this->name, 0, 5, 'UTF-8') . '...';
			}

		return '' . $str . '';
		}

	public

	function GetLink()
		{
		return '<a name="des' . $this->id . '">' . $this->name . '</a>';
		}

	public

	function SetName($name)
		{
		$this->name = $name;
		}

	public

	function GetNormalOdd($week, $trainigs, $br, $been)
		{
		$str = '';
		if ($trainigs % 2 == 0 && $this->freq == 1)
			{
			if ($this->id == 1)
				{

				// rovimas

				if (!empty($this->const1))
					{
					if ($been % $this->const1 != 1 || $been == 1)
						{
						$part1 = round($this->all / 4);
						$part2 = round($this->all / 2);
						$part3 = round(($this->all / 4) * 3);
						if ($been < $part1)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
							}
						elseif ($part1 <= $been && $been < $part2)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
							}
						elseif ($part2 <= $been && $been < $part3)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
							}
						elseif ($part3 <= $been)
							{
							$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
							}

						$str.= ' ' . $kgprc . ' ';
						}
					}
				  else
					{
					$part1 = round($this->all / 4);
					$part2 = round($this->all / 2);
					$part3 = round(($this->all / 4) * 3);
					if ($been < $part1)
						{
						$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
						}
					elseif ($part1 <= $been && $been < $part2)
						{
						$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
						}
					elseif ($part2 <= $been && $been < $part3)
						{
						$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning[90];
						}
					elseif ($part3 <= $been)
						{
						$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
						}

					$str.= ' ' . $kgprc . ' ';
					}
				}
			elseif ($this->id == 2)
				{

				// rovimas2

				if (!empty($this->const1))
					{
					if ($been % $this->const1 == 1 && $been != 1)
						{
						$part1 = round($this->all / 4);
						$part2 = round($this->all / 2);
						$part3 = round(($this->all / 4) * 3);
						if ($been < $part1)
							{
							$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
							}
						elseif ($part1 <= $been && $been < $part2)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning[80];
							}
						elseif ($part2 <= $been && $been < $part3)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning[85];
							}
						elseif ($part3 <= $been)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.90 * $this->mass)) . 'kg  ' . $br . $this->planning[90];
							}

						$str.= ' ' . $kgprc . ' ';
						}
					}
				}
			elseif ($this->id == 7)
				{

				// trauka stumimo

				$part1 = round($this->all / 3);
				$part2 = round(($this->all / 3) * 2);
				if ($been < $part1)
					{
					$kgprc = ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 105% - ' . $this->calibrate(round(1.05 * $this->mass)) . 'kg ' . $br . $this->planning4[105];
					}
				elseif ($part2 <= $been)
					{
					$kgprc = ' 110% - ' . $this->calibrate(round(1.1 * $this->mass)) . 'kg ' . $br . $this->planning4[110];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 8)
				{

				// stumimas nuo krutines

				$part1 = round($this->all / 4);
				$part2 = round($this->all / 2);
				$part3 = round(($this->all / 4) * 3);
				if ($been < $part1)
					{
					$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
					}
				elseif ($part2 <= $been && $been < $part3)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning3[85];
					}
				elseif ($part3 <= $been)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.90 * $this->mass)) . 'kg  ' . $br . $this->planning3[90];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 11)
				{

				// pritupimai ant nugaros

				$part1 = round($this->all / 4);
				$part2 = round($this->all / 2);
				$part3 = round(($this->all / 4) * 3);
				if ($been < $part1)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning5[85];
					}
				elseif ($part2 <= $been && $been < $part3)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning5[90];
					}
				elseif ($part3 <= $been)
					{
					$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg  ' . $br . $this->planning5[95];
					}

				$str.= ' ' . $kgprc;
				}
			}
		elseif ($trainigs % 2 == 1 && $this->freq == 2)
			{
			if ($this->id == 4)
				{

				// stumimas <br/>

				if (!empty($this->const2) && !empty($this->const3))
					{
					if (in_array($been, $this->mas1))
						{
						$str.= '' . $this->mas[$been]['procents'] . '% - ' . $this->calibrate(round(($this->mas[$been]['procents'] / 100) * $this->mass)) . ' kg ' . $br . $this->mas[$been]['planning'];
						}
					}
				  else
					{
					$str.= '' . $this->mas[$been]['procents'] . '% - ' . $this->calibrate(round(($this->mas[$been]['procents'] / 100) * $this->mass)) . ' kg ' . $br . $this->mas[$been]['planning'];
					}
				}
			elseif ($this->id == 3)
				{

				// trauka rovimo

				$part1 = round($this->all / 3);
				$part2 = round(($this->all / 3) * 2);
				if ($been < $part1)
					{
					$kgprc = ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 105% - ' . $this->calibrate(round(1.05 * $this->mass)) . 'kg  ' . $br . $this->planning4[105];
					}
				elseif ($part2 <= $been)
					{
					$kgprc = ' 110% - ' . $this->calibrate(round(1.1 * $this->mass)) . 'kg  ' . $br . $this->planning4[110];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 5)
				{

				// stumimas2

				if (!empty($this->const2))
					{
					if (in_array($been, $this->mas2))
						{
						$str.= '' . $this->mas[$been]['procents'] . '% - ' . $this->calibrate(round(($this->mas[$been]['procents'] / 100) * $this->mass)) . ' kg ' . $br . $this->mas[$been]['planning'];
						}
					}
				}
			elseif ($this->id == 6)
				{

				// stumimas3

				if (!empty($this->const3))
					{
					if (in_array($been, $this->mas3))
						{
						$str.= '' . $this->mas[$been]['procents'] . '% - ' . $this->calibrate(round(($this->mas[$been]['procents'] / 100) * $this->mass)) . ' kg ' . $br . $this->mas[$been]['planning'];
						}
					}
				}
			elseif ($this->id == 9)
				{

				// stumimas nuo nugaros

				$part1 = round($this->all / 4);
				$part2 = round($this->all / 2);
				$part3 = round(($this->all / 4) * 3);
				if ($been < $part1)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning3[85];
					}
				elseif ($part2 <= $been && $been < $part3)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning3[90];
					}
				elseif ($part3 <= $been)
					{
					$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg  ' . $br . $this->planning3[95];
					}

				$str.= ' ' . $kgprc;
				}
			elseif ($this->id == 10)
				{

				// pritupimai ant krutines

				$part1 = round($this->all / 4);
				$part2 = round($this->all / 2);
				$part3 = round(($this->all / 4) * 3);
				if ($been < $part1)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning5[85];
					}
				elseif ($part2 <= $been && $been < $part3)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning5[90];
					}
				elseif ($part3 <= $been)
					{
					$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg  ' . $br . $this->planning5[95];
					}

				$str.= ' ' . $kgprc;
				}
			}

		return $str;
		}

	public

	function GetNormalOdd75($week, $trainigs, $br, $been)
		{
		$str = '';
		if ($trainigs % 2 == 0 && $this->freq == 1)
			{
			if ($this->id == 1)
				{

				// rovimas

				if (!empty($this->const1))
					{
					if ($been % $this->const1 != 1 || $been == 1)
						{
						$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
						$str.= ' ' . $kgprc . ' ';
						}
					}
				  else
					{
					$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
					$str.= ' ' . $kgprc . ' ';
					}
				}
			elseif ($this->id == 2)
				{

				// rovimas2

				if (!empty($this->const1))
					{
					if ($been % $this->const1 == 1 && $been != 1)
						{
						$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
						$str.= ' ' . $kgprc . ' ';
						}
					}
				}
			elseif ($this->id == 7)
				{

				// trauka stumimo

				$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning4[90];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 8)
				{

				// stumimas nuo krutines

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 11)
				{

				// pritupimai ant nugaros

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning5[75];
				$str.= ' ' . $kgprc;
				}
			}
		elseif ($trainigs % 2 == 1 && $this->freq == 2)
			{
			if ($this->id == 4)
				{

				// stumimas <br/>

				$str.= ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
				}
			elseif ($this->id == 3)
				{

				// trauka rovimo

				$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning4[90];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 5)
				{

				// stumimas2

				$str.= ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
				}
			elseif ($this->id == 6)
				{

				// stumimas3

				$str.= ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
				}
			elseif ($this->id == 9)
				{

				// stumimas nuo nugaros

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
				$str.= ' ' . $kgprc;
				}
			elseif ($this->id == 10)
				{

				// pritupimai ant krutines

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning5[75];
				$str.= ' ' . $kgprc;
				}
			}

		return $str;
		}

	public

	function GetNormalOdd100($week, $trainigs, $br, $been)
		{
		$str = '';
		if ($trainigs % 2 == 0 && $this->freq == 1)
			{
			if ($this->id == 1)
				{

				// rovimas

				if (!empty($this->const1))
					{
					if ($been % $this->const1 != 1 || $been == 1)
						{
						$kgprc = 'Asm. rekordas   ' . $br . $this->planning['max'];
						$str.= ' ' . $kgprc . ' ';
						}
					}
				  else
					{
					$kgprc = 'Asm. rekordas   ' . $br . $this->planning['max'];
					$str.= ' ' . $kgprc . ' ';
					}
				}
			elseif ($this->id == 2)
				{

				// rovimas2

				if (!empty($this->const1))
					{
					if ($been % $this->const1 == 1 && $been != 1)
						{
						$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
						$str.= ' ' . $kgprc . ' ';
						}
					}
				}
			elseif ($this->id == 7)
				{

				// trauka stumimo

				$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning4[90];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 8)
				{

				// stumimas nuo krutines

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 11)
				{

				// pritupimai ant nugaros

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning5[75];
				$str.= ' ' . $kgprc;
				}
			}
		elseif ($trainigs % 2 == 1 && $this->freq == 2)
			{
			if ($this->id == 4)
				{

				// stumimas <br/>

				$str.= 'Asm. rekordas   ' . $br . $this->planning['max'];
				}
			elseif ($this->id == 3)
				{

				// trauka rovimo

				$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning4[90];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 5)
				{

				// stumimas2

				$str.= ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
				}
			elseif ($this->id == 6)
				{

				// stumimas3

				$str.= ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
				}
			elseif ($this->id == 9)
				{

				// stumimas nuo nugaros

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
				$str.= ' ' . $kgprc;
				}
			elseif ($this->id == 10)
				{

				// pritupimai ant krutines

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning5[75];
				$str.= ' ' . $kgprc;
				}
			}

		return $str;
		}

	public

	function GetNormalOddStatic($week, $trainigs, $br, $been)
		{
		$str = '';
		if ($trainigs % 2 == 0 && $this->freq == 1)
			{
			if ($this->id == 1)
				{

				// rovimas

				if (!empty($this->const1))
					{
					if ($been % $this->const1 != 1 || $been == 1)
						{
						$str.= ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
						}
					}
				  else
					{
					$str.= ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
					}
				}
			elseif ($this->id == 2)
				{

				// rovimas2

				if (!empty($this->const1))
					{
					if ($been % $this->const1 == 1 && $been != 1)
						{
						$str.= $kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning[80];
						}
					}
				}
			elseif ($this->id == 7)
				{

				// trauka stumimo

				$str.= ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
				}
			elseif ($this->id == 8)
				{

				// stumimas nuo krutines

				$str.= ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
				}
			elseif ($this->id == 11)
				{

				// pritupimai ant nugaros

				$str.= ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
				}
			}
		elseif ($trainigs % 2 == 1 && $this->freq == 2)
			{
			if ($this->id == 4)
				{

				// stumimas <br/>

				if (!empty($this->const2) && !empty($this->const3))
					{
					if (in_array($been, $this->mas1))
						{
						$str.= '80% - ' . $this->calibrate(round(0.8 * $this->mass)) . ' kg ' . $br . $this->planning[80];
						}
					}
				  else
					{
					$str.= '80% - ' . $this->calibrate(round(0.8 * $this->mass)) . ' kg ' . $br . $this->planning[80];
					}
				}
			elseif ($this->id == 3)
				{

				// trauka rovimo

				$str.= ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
				}
			elseif ($this->id == 5)
				{

				// stumimas2

				if (!empty($this->const2))
					{
					if (in_array($been, $this->mas2))
						{
						$str.= '80% - ' . $this->calibrate(round(0.8 * $this->mass)) . ' kg ' . $br . $this->planning[80];
						}
					}
				}
			elseif ($this->id == 6)
				{

				// stumimas3

				if (!empty($this->const3))
					{
					if (in_array($been, $this->mas3))
						{
						$str.= '80% - ' . $this->calibrate(round(0.8 * $this->mass)) . ' kg ' . $br . $this->planning[80];
						}
					}
				}
			elseif ($this->id == 9)
				{

				// stumimas nuo nugaros

				$str.= ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
				}
			elseif ($this->id == 10)
				{

				// pritupimai ant krutines

				$str.= ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
				}
			}

		return $str;
		}

	public

	function GetNormalOddBio($week, $trainigs, $br, $been, $physical)
		{
		$str = '';
		if ($trainigs % 2 == 0 && $this->freq == 1)
			{
			if ($this->id == 1)
				{

				// rovimas

				if (!empty($this->const1))
					{
					if ($been % $this->const1 != 1 || $been == 1)
						{
						if ($physical < - 0.5)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
							}
						elseif (-0.5 <= $physical && $physical < 0)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
							}
						elseif (0 <= $physical && $physical < 0.5)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
							}
						elseif ($physical >= 0.5)
							{
							$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
							}

						$str.= ' ' . $kgprc . ' ';
						}
					}
				  else
					{
					if ($physical < - 0.5)
						{
						$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
						}
					elseif (-0.5 <= $physical && $physical < 0)
						{
						$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
						}
					elseif (0 <= $physical && $physical < 0.5)
						{
						$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
						}
					elseif ($physical >= 0.5)
						{
						$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
						}

					$str.= ' ' . $kgprc . ' ';
					}
				}
			elseif ($this->id == 2)
				{

				// rovimas2

				if (!empty($this->const1))
					{
					if ($been % $this->const1 == 1 && $been != 1)
						{
						if ($physical < - 0.5)
							{
							$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
							}
						elseif (-0.5 <= $physical && $physical < 0)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning[80];
							}
						elseif (0 <= $physical && $physical < 0.5)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning[85];
							}
						elseif ($physical >= 0.5)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.90 * $this->mass)) . 'kg  ' . $br . $this->planning[90];
							}

						$str.= ' ' . $kgprc . ' ';
						}
					}
				}
			elseif ($this->id == 7)
				{

				// trauka stumimo

				if ($physical < - 0.66)
					{
					$kgprc = ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
					}
				elseif (-0.66 <= $physical && $physical < 0.66)
					{
					$kgprc = ' 105% - ' . $this->calibrate(round(1.05 * $this->mass)) . 'kg ' . $br . $this->planning4[105];
					}
				elseif ($physical >= 0.66)
					{
					$kgprc = ' 110% - ' . $this->calibrate(round(1.1 * $this->mass)) . 'kg ' . $br . $this->planning4[110];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 8)
				{

				// stumimas nuo krutines

				if ($physical < - 0.5)
					{
					$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
					}
				elseif (-0.5 <= $physical && $physical < 0)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
					}
				elseif (0 <= $physical && $physical < 0.5)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning3[85];
					}
				elseif ($physical >= 0.5)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.90 * $this->mass)) . 'kg  ' . $br . $this->planning3[90];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 11)
				{

				// pritupimai ant nugaros

				if ($physical < - 0.5)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
					}
				elseif (-0.5 <= $physical && $physical < 0)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning5[85];
					}
				elseif (0 <= $physical && $physical < 0.5)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning5[90];
					}
				elseif ($physical >= 0.5)
					{
					$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg  ' . $br . $this->planning5[95];
					}

				$str.= ' ' . $kgprc;
				}
			}
		elseif ($trainigs % 2 == 1 && $this->freq == 2)
			{
			if ($this->id == 4)
				{

				// stumimas <br/>

				if (!empty($this->const2) && !empty($this->const3))
					{
					if (in_array($been, $this->mas1))
						{
						if ($physical < - 0.5)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
							}
						elseif (-0.5 <= $physical && $physical < 0)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
							}
						elseif (0 <= $physical && $physical < 0.5)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
							}
						elseif ($physical >= 0.5)
							{
							$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
							}

						$str.= ' ' . $kgprc . '  ';
						}
					}
				  else
					{
					if ($physical < - 0.5)
						{
						$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
						}
					elseif (-0.5 <= $physical && $physical < 0)
						{
						$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
						}
					elseif (0 <= $physical && $physical < 0.5)
						{
						$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
						}
					elseif ($physical >= 0.5)
						{
						$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
						}

					$str.= ' ' . $kgprc . ' ';
					}
				}
			elseif ($this->id == 3)
				{

				// trauka rovimo

				if ($physical < - 0.66)
					{
					$kgprc = ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
					}
				elseif (-0.66 <= $physical && $physical < 0.66)
					{
					$kgprc = ' 105% - ' . $this->calibrate(round(1.05 * $this->mass)) . 'kg ' . $br . $this->planning4[105];
					}
				elseif ($physical >= 0.66)
					{
					$kgprc = ' 110% - ' . $this->calibrate(round(1.1 * $this->mass)) . 'kg ' . $br . $this->planning4[110];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 5)
				{

				// stumimas2

				if (!empty($this->const2))
					{
					if (in_array($been, $this->mas2))
						{
						if ($physical < - 0.5)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
							}
						elseif (-0.5 <= $physical && $physical < 0)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
							}
						elseif (0 <= $physical && $physical < 0.5)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
							}
						elseif ($physical >= 0.5)
							{
							$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
							}

						$str.= ' ' . $kgprc . ' ';
						}
					}
				}
			elseif ($this->id == 6)
				{

				// stumimas3

				if (!empty($this->const3))
					{
					if (in_array($been, $this->mas3))
						{
						if ($physical < - 0.5)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
							}
						elseif (-0.5 <= $physical && $physical < 0)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
							}
						elseif (0 <= $physical && $physical < 0.5)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
							}
						elseif ($physical >= 0.5)
							{
							$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
							}

						$str.= ' ' . $kgprc . ' ';
						}
					}
				}
			elseif ($this->id == 9)
				{

				// stumimas nuo nugaros

				if ($physical < - 0.5)
					{
					$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
					}
				elseif (-0.5 <= $physical && $physical < 0)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
					}
				elseif (0 <= $physical && $physical < 0.5)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning3[85];
					}
				elseif ($physical >= 0.5)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.90 * $this->mass)) . 'kg  ' . $br . $this->planning3[90];
					}

				$str.= ' ' . $kgprc;
				}
			elseif ($this->id == 10)
				{

				// pritupimai ant krutines

				if ($physical < - 0.5)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
					}
				elseif (-0.5 <= $physical && $physical < 0)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning5[85];
					}
				elseif (0 <= $physical && $physical < 0.5)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning5[90];
					}
				elseif ($physical >= 0.5)
					{
					$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg  ' . $br . $this->planning5[95];
					}

				$str.= ' ' . $kgprc;
				}
			}

		return $str;
		}

	public

	function GetNormalEven($week, $trainigs, $br, $been)
		{
		$str = '';
		if ($trainigs % 2 == 1 && $this->freq == 1)
			{
			if ($this->id == 1)
				{

				// rovimas

				if (!empty($this->const1))
					{
					if ($been % $this->const1 != 1 || $been == 1)
						{
						$part1 = round($this->all / 4);
						$part2 = round($this->all / 2);
						$part3 = round(($this->all / 4) * 3);
						if ($been < $part1)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
							}
						elseif ($part1 <= $been && $been < $part2)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
							}
						elseif ($part2 <= $been && $been < $part3)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
							}
						elseif ($part3 <= $been)
							{
							$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
							}

						$str.= ' ' . $kgprc . ' ';
						}
					}
				  else
					{
					$part1 = round($this->all / 4);
					$part2 = round($this->all / 2);
					$part3 = round(($this->all / 4) * 3);
					if ($been < $part1)
						{
						$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
						}
					elseif ($part1 <= $been && $been < $part2)
						{
						$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
						}
					elseif ($part2 <= $been && $been < $part3)
						{
						$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning[90];
						}
					elseif ($part3 <= $been)
						{
						$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
						}

					$str.= ' ' . $kgprc . ' ';
					}
				}
			elseif ($this->id == 2)
				{

				// rovimas2

				if (!empty($this->const1))
					{
					if ($been % $this->const1 == 1 && $been != 1)
						{
						$part1 = round($this->all / 4);
						$part2 = round($this->all / 2);
						$part3 = round(($this->all / 4) * 3);
						if ($been < $part1)
							{
							$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
							}
						elseif ($part1 <= $been && $been < $part2)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning[80];
							}
						elseif ($part2 <= $been && $been < $part3)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning[85];
							}
						elseif ($part3 <= $been)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.90 * $this->mass)) . 'kg  ' . $br . $this->planning[90];
							}

						$str.= ' ' . $kgprc . ' ';
						}
					}
				}
			elseif ($this->id == 7)
				{

				// trauka stumimo

				$part1 = round($this->all / 3);
				$part2 = round(($this->all / 3) * 2);
				if ($been < $part1)
					{
					$kgprc = ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 105% - ' . $this->calibrate(round(1.05 * $this->mass)) . 'kg ' . $br . $this->planning4[105];
					}
				elseif ($part2 <= $been)
					{
					$kgprc = ' 110% - ' . $this->calibrate(round(1.1 * $this->mass)) . 'kg ' . $br . $this->planning4[110];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 8)
				{

				// stumimas nuo krutines

				$part1 = round($this->all / 4);
				$part2 = round($this->all / 2);
				$part3 = round(($this->all / 4) * 3);
				if ($been < $part1)
					{
					$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
					}
				elseif ($part2 <= $been && $been < $part3)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning3[85];
					}
				elseif ($part3 <= $been)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.90 * $this->mass)) . 'kg  ' . $br . $this->planning3[90];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 11)
				{

				// pritupimai ant nugaros

				$part1 = round($this->all / 4);
				$part2 = round($this->all / 2);
				$part3 = round(($this->all / 4) * 3);
				if ($been < $part1)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning5[85];
					}
				elseif ($part2 <= $been && $been < $part3)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning5[90];
					}
				elseif ($part3 <= $been)
					{
					$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg  ' . $br . $this->planning5[95];
					}

				$str.= ' ' . $kgprc;
				}
			}
		elseif ($trainigs % 2 == 0 && $this->freq == 2)
			{
			if ($this->id == 4)
				{

				// stumimas <br/>

				if (!empty($this->const2) && !empty($this->const3))
					{
					if (in_array($been, $this->mas1))
						{
						$str.= '' . $this->mas[$been]['procents'] . '% - ' . $this->calibrate(round(($this->mas[$been]['procents'] / 100) * $this->mass)) . ' kg ' . $br . $this->mas[$been]['planning'];
						}
					}
				  else
					{
					$str.= '' . $this->mas[$been]['procents'] . '% - ' . $this->calibrate(round(($this->mas[$been]['procents'] / 100) * $this->mass)) . ' kg ' . $br . $this->mas[$been]['planning'];
					}
				}
			elseif ($this->id == 3)
				{

				// trauka rovimo

				$part1 = round($this->all / 3);
				$part2 = round(($this->all / 3) * 2);
				if ($been < $part1)
					{
					$kgprc = ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 105% - ' . $this->calibrate(round(1.05 * $this->mass)) . 'kg  ' . $br . $this->planning4[105];
					}
				elseif ($part2 <= $been)
					{
					$kgprc = ' 110% - ' . $this->calibrate(round(1.1 * $this->mass)) . 'kg  ' . $br . $this->planning4[110];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 5)
				{

				// stumimas2

				if (!empty($this->const2))
					{
					if (in_array($been, $this->mas2))
						{
						$str.= '' . $this->mas[$been]['procents'] . '% - ' . $this->calibrate(round(($this->mas[$been]['procents'] / 100) * $this->mass)) . ' kg ' . $br . $this->mas[$been]['planning'];
						}
					}
				}
			elseif ($this->id == 6)
				{

				// stumimas3

				if (!empty($this->const3))
					{
					if (in_array($been, $this->mas3))
						{
						$str.= '' . $this->mas[$been]['procents'] . '% - ' . $this->calibrate(round(($this->mas[$been]['procents'] / 100) * $this->mass)) . ' kg ' . $br . $this->mas[$been]['planning'];
						}
					}
				}
			elseif ($this->id == 9)
				{

				// stumimas nuo nugaros

				$part1 = round($this->all / 4);
				$part2 = round($this->all / 2);
				$part3 = round(($this->all / 4) * 3);
				if ($been < $part1)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning3[85];
					}
				elseif ($part2 <= $been && $been < $part3)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning3[90];
					}
				elseif ($part3 <= $been)
					{
					$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg  ' . $br . $this->planning3[95];
					}

				$str.= ' ' . $kgprc;
				}
			elseif ($this->id == 10)
				{

				// pritupimai ant krutines

				$part1 = round($this->all / 4);
				$part2 = round($this->all / 2);
				$part3 = round(($this->all / 4) * 3);
				if ($been < $part1)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
					}
				elseif ($part1 <= $been && $been < $part2)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning5[85];
					}
				elseif ($part2 <= $been && $been < $part3)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning5[90];
					}
				elseif ($part3 <= $been)
					{
					$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg  ' . $br . $this->planning5[95];
					}

				$str.= ' ' . $kgprc;
				}
			}

		return $str;
		}

	public

	function GetNormalEven75($week, $trainigs, $br, $been)
		{
		$str = '';
		if ($trainigs % 2 == 1 && $this->freq == 1)
			{
			if ($this->id == 1)
				{

				// rovimas

				if (!empty($this->const1))
					{
					if ($been % $this->const1 != 1 || $been == 1)
						{
						$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
						$str.= ' ' . $kgprc . ' ';
						}
					}
				  else
					{
					$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
					$str.= ' ' . $kgprc . ' ';
					}
				}
			elseif ($this->id == 2)
				{

				// rovimas2

				if (!empty($this->const1))
					{
					if ($been % $this->const1 == 1 && $been != 1)
						{
						$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
						$str.= ' ' . $kgprc . ' ';
						}
					}
				}
			elseif ($this->id == 7)
				{

				// trauka stumimo

				$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning4[90];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 8)
				{

				// stumimas nuo krutines

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 11)
				{

				// pritupimai ant nugaros

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning5[75];
				$str.= ' ' . $kgprc;
				}
			}
		elseif ($trainigs % 2 == 0 && $this->freq == 2)
			{
			if ($this->id == 4)
				{

				// stumimas <br/>

				$str.= ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
				}
			elseif ($this->id == 3)
				{

				// trauka rovimo

				$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning4[90];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 5)
				{

				// stumimas2

				$str.= ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
				}
			elseif ($this->id == 6)
				{

				// stumimas3

				$str.= ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
				}
			elseif ($this->id == 9)
				{

				// stumimas nuo nugaros

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
				$str.= ' ' . $kgprc;
				}
			elseif ($this->id == 10)
				{

				// pritupimai ant krutines

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning5[75];
				$str.= ' ' . $kgprc;
				}
			}

		return $str;
		}

	public

	function GetNormalEvenStatic($week, $trainigs, $br, $been)
		{
		$str = '';
		if ($trainigs % 2 == 1 && $this->freq == 1)
			{
			if ($this->id == 1)
				{

				// rovimas

				if (!empty($this->const1))
					{
					if ($been % $this->const1 != 1 || $been == 1)
						{
						$str.= ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
						}
					}
				  else
					{
					$str.= ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
					}
				}
			elseif ($this->id == 2)
				{

				// rovimas2

				if (!empty($this->const1))
					{
					if ($been % $this->const1 == 1 && $been != 1)
						{
						$str.= $kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning[80];
						}
					}
				}
			elseif ($this->id == 7)
				{

				// trauka stumimo

				$str.= ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
				}
			elseif ($this->id == 8)
				{

				// stumimas nuo krutines

				$str.= ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
				}
			elseif ($this->id == 11)
				{

				// pritupimai ant nugaros

				$str.= ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
				}
			}
		elseif ($trainigs % 2 == 0 && $this->freq == 2)
			{
			if ($this->id == 4)
				{

				// stumimas <br/>

				if (!empty($this->const2) && !empty($this->const3))
					{
					if (in_array($been, $this->mas1))
						{
						$str.= '80% - ' . $this->calibrate(round(0.8 * $this->mass)) . ' kg ' . $br . $this->planning[80];
						}
					}
				  else
					{
					$str.= '80% - ' . $this->calibrate(round(0.8 * $this->mass)) . ' kg ' . $br . $this->planning[80];
					}
				}
			elseif ($this->id == 3)
				{

				// trauka rovimo

				$str.= ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
				}
			elseif ($this->id == 5)
				{

				// stumimas2

				if (!empty($this->const2))
					{
					if (in_array($been, $this->mas2))
						{
						$str.= '80% - ' . $this->calibrate(round(0.8 * $this->mass)) . ' kg ' . $br . $this->planning[80];
						}
					}
				}
			elseif ($this->id == 6)
				{

				// stumimas3

				if (!empty($this->const3))
					{
					if (in_array($been, $this->mas3))
						{
						$str.= '80% - ' . $this->calibrate(round(0.8 * $this->mass)) . ' kg ' . $br . $this->planning[80];
						}
					}
				}
			elseif ($this->id == 9)
				{

				// stumimas nuo nugaros

				$str.= ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
				}
			elseif ($this->id == 10)
				{

				// pritupimai ant krutines

				$str.= ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
				}
			}

		return $str;
		}

	public

	function GetNormalEven100($week, $trainigs, $br, $been)
		{
		$str = '';
		if ($trainigs % 2 == 1 && $this->freq == 1)
			{
			if ($this->id == 1)
				{

				// rovimas

				if (!empty($this->const1))
					{
					if ($been % $this->const1 != 1 || $been == 1)
						{
						$str.= 'Asm. rekordas   ' . $br . $this->planning['max'];
						}
					}
				  else
					{
					$str.= 'Asm. rekordas   ' . $br . $this->planning['max'];
					}
				}
			elseif ($this->id == 2)
				{

				// rovimas2

				if (!empty($this->const1))
					{
					if ($been % $this->const1 == 1 && $been != 1)
						{
						$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
						$str.= ' ' . $kgprc . ' ';
						}
					}
				}
			elseif ($this->id == 7)
				{

				// trauka stumimo

				$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning4[90];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 8)
				{

				// stumimas nuo krutines

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 11)
				{

				// pritupimai ant nugaros

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning5[75];
				$str.= ' ' . $kgprc;
				}
			}
		elseif ($trainigs % 2 == 0 && $this->freq == 2)
			{
			if ($this->id == 4)
				{

				// stumimas <br/>

				$str.= 'Asm. rekordas   ' . $br . $this->planning['max'];
				}
			elseif ($this->id == 3)
				{

				// trauka rovimo

				$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning4[90];
				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 5)
				{

				// stumimas2

				$str.= ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
				}
			elseif ($this->id == 6)
				{

				// stumimas3

				$str.= ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
				}
			elseif ($this->id == 9)
				{

				// stumimas nuo nugaros

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
				$str.= ' ' . $kgprc;
				}
			elseif ($this->id == 10)
				{

				// pritupimai ant krutines

				$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning5[75];
				$str.= ' ' . $kgprc;
				}
			}

		return $str;
		}

	public

	function GetNormalEvenBio($week, $trainigs, $br, $been, $physical)
		{
		$str = '';
		if ($trainigs % 2 == 1 && $this->freq == 1)
			{
			if ($this->id == 1)
				{

				// rovimas

				if (!empty($this->const1))
					{
					if ($been % $this->const1 != 1 || $been == 1)
						{
						if ($physical < - 0.5)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
							}
						elseif (-0.5 <= $physical && $physical < 0)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
							}
						elseif (0 <= $physical && $physical < 0.5)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
							}
						elseif ($physical >= 0.5)
							{
							$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
							}

						$str.= ' ' . $kgprc . ' ';
						}
					}
				  else
					{
					if ($physical < - 0.5)
						{
						$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
						}
					elseif (-0.5 <= $physical && $physical < 0)
						{
						$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
						}
					elseif (0 <= $physical && $physical < 0.5)
						{
						$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
						}
					elseif ($part3 >= 0.5)
						{
						$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
						}

					$str.= ' ' . $kgprc . ' ';
					}
				}
			elseif ($this->id == 2)
				{

				// rovimas2

				if (!empty($this->const1))
					{
					if ($been % $this->const1 == 1 && $been != 1)
						{
						if ($physical < - 0.5)
							{
							$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg ' . $br . $this->planning[75];
							}
						elseif (-0.5 <= $physical && $physical < 0)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning[80];
							}
						elseif (0 <= $physical && $physical < 0.5)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning[85];
							}
						elseif ($physical >= 0.5)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.90 * $this->mass)) . 'kg  ' . $br . $this->planning[90];
							}

						$str.= ' ' . $kgprc . ' ';
						}
					}
				}
			elseif ($this->id == 7)
				{

				// trauka stumimo

				if ($physical < - 0.66)
					{
					$kgprc = ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
					}
				elseif (-0.66 <= $physical && $physical < 0.66)
					{
					$kgprc = ' 105% - ' . $this->calibrate(round(1.05 * $this->mass)) . 'kg ' . $br . $this->planning4[105];
					}
				elseif ($physical >= 0.66)
					{
					$kgprc = ' 110% - ' . $this->calibrate(round(1.1 * $this->mass)) . 'kg ' . $br . $this->planning4[110];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 8)
				{

				// stumimas nuo krutines

				if ($physical < - 0.5)
					{
					$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
					}
				elseif (-0.5 <= $physical && $physical < 0)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
					}
				elseif (0 <= $physical && $physical < 0.5)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning3[85];
					}
				elseif ($physical >= 0.5)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.90 * $this->mass)) . 'kg  ' . $br . $this->planning3[90];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 11)
				{

				// pritupimai ant nugaros

				if ($physical < - 0.5)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
					}
				elseif (-0.5 <= $physical && $physical < 0)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning5[85];
					}
				elseif (0 <= $physical && $physical < 0.5)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning5[90];
					}
				elseif ($physical >= 0.5)
					{
					$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg  ' . $br . $this->planning5[95];
					}

				$str.= ' ' . $kgprc;
				}
			}
		elseif ($trainigs % 2 == 0 && $this->freq == 2)
			{
			if ($this->id == 4)
				{

				// stumimas <br/>

				if (!empty($this->const2) && !empty($this->const3))
					{
					if (in_array($been, $this->mas1))
						{
						if ($physical < - 0.5)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
							}
						elseif (-0.5 <= $physical && $physical < 0)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
							}
						elseif (0 <= $physical && $physical < 0.5)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
							}
						elseif ($physical >= 0.5)
							{
							$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
							}

						$str.= ' ' . $kgprc . '  ';
						}
					}
				  else
					{
					if ($physical < - 0.5)
						{
						$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
						}
					elseif (-0.5 <= $physical && $physical < 0)
						{
						$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
						}
					elseif (0 <= $physical && $physical < 0.5)
						{
						$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
						}
					elseif ($part3 >= 0.5)
						{
						$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
						}

					$str.= ' ' . $kgprc . '  ';
					}
				}
			elseif ($this->id == 3)
				{

				// trauka rovimo

				if ($physical < - 0.66)
					{
					$kgprc = ' 100% - ' . $this->calibrate(round(1 * $this->mass)) . 'kg  ' . $br . $this->planning4[100];
					}
				elseif (-0.66 <= $physical && $physical < 0.66)
					{
					$kgprc = ' 105% - ' . $this->calibrate(round(1.05 * $this->mass)) . 'kg ' . $br . $this->planning4[105];
					}
				elseif ($physical >= 0.66)
					{
					$kgprc = ' 110% - ' . $this->calibrate(round(1.1 * $this->mass)) . 'kg ' . $br . $this->planning4[110];
					}

				$str.= ' ' . $kgprc . ' ';
				}
			elseif ($this->id == 5)
				{

				// stumimas2

				if (!empty($this->const2))
					{
					if (in_array($been, $this->mas2))
						{
						if ($physical < - 0.5)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
							}
						elseif (-0.5 <= $physical && $physical < 0)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
							}
						elseif (0 <= $physical && $physical < 0.5)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
							}
						elseif ($physical >= 0.5)
							{
							$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
							}

						$str.= ' ' . $kgprc . ' ';
						}
					}
				}
			elseif ($this->id == 6)
				{

				// stumimas3

				if (!empty($this->const3))
					{
					if (in_array($been, $this->mas3))
						{
						if ($physical < - 0.5)
							{
							$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg ' . $br . $this->planning[80];
							}
						elseif (-0.5 <= $physical && $physical < 0)
							{
							$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg ' . $br . $this->planning[85];
							}
						elseif (0 <= $physical && $physical < 0.5)
							{
							$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg ' . $br . $this->planning[90];
							}
						elseif ($physical >= 0.5)
							{
							$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg ' . $br . $this->planning[95];
							}

						$str.= ' ' . $kgprc . '  ';
						}
					}
				}
			elseif ($this->id == 9)
				{

				// stumimas nuo nugaros

				if ($physical < - 0.5)
					{
					$kgprc = ' 75% - ' . $this->calibrate(round(0.75 * $this->mass)) . 'kg  ' . $br . $this->planning3[75];
					}
				elseif (-0.5 <= $physical && $physical < 0)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.80 * $this->mass)) . 'kg  ' . $br . $this->planning3[80];
					}
				elseif (0 <= $physical && $physical < 0.5)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning3[85];
					}
				elseif ($physical >= 0.5)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.90 * $this->mass)) . 'kg  ' . $br . $this->planning3[90];
					}

				$str.= ' ' . $kgprc;
				}
			elseif ($this->id == 10)
				{

				// pritupimai ant krutines

				if ($physical < - 0.5)
					{
					$kgprc = ' 80% - ' . $this->calibrate(round(0.8 * $this->mass)) . 'kg  ' . $br . $this->planning5[80];
					}
				elseif (-0.5 <= $physical && $physical < 0)
					{
					$kgprc = ' 85% - ' . $this->calibrate(round(0.85 * $this->mass)) . 'kg  ' . $br . $this->planning5[85];
					}
				elseif (0 <= $physical && $physical < 0.5)
					{
					$kgprc = ' 90% - ' . $this->calibrate(round(0.9 * $this->mass)) . 'kg  ' . $br . $this->planning5[90];
					}
				elseif ($physical >= 0.5)
					{
					$kgprc = ' 95% - ' . $this->calibrate(round(0.95 * $this->mass)) . 'kg  ' . $br . $this->planning5[95];
					}

				$str.= ' ' . $kgprc;
				}
			}

		return $str;
		}

	public

	function Calc($week, $trainigs, $t, $br = '<br/>')
		{
		$week++;
		$trainigs++;
		$been = ($week - 1) * $t + $trainigs;

		// $str=' '.$week.' '.$trainigs.' '.$this->freq.' '.$this->id.' ';
		// $str=' '.$been.' '.($trainigs%2);

		$str = $this->GetNormalEven($week, $trainigs, $br, $been);
		return ' ' . $str . ' ';
		}

	public

	function Calc3($week, $trainigs, $t, $br = '<br/>')
		{
		$week++;
		$trainigs++;
		$been = ($week - 1) * $t + $trainigs;

		// $str=' '.$week.' '.$trainigs.' '.$this->freq.' '.$this->id.' ';
		// $str=' '.$been.' '.($trainigs%2);

		if ($week < $this->weeks - 1)
			{
			$str = $this->GetNormalEven($week, $trainigs, $br, $been);
			}
		elseif ($week == $this->weeks - 1)
			{
			$str = $this->GetNormalEven100($week, $trainigs, $br, $been);
			}
		elseif ($week == $this->weeks)
			{
			$str = $this->GetNormalEven75($week, $trainigs, $br, $been);
			}

		return ' ' . $str . ' ';
		}

	public

	function Calc5($week, $trainigs, $t, $days, $br = '<br/>')
		{
		$week++;
		$trainigs++;
		$rest = (7 - $this->trainigs) * $week;
		$been = ($week - 1) * $t + $trainigs;
		$godays = $rest + $been;
		$calc_days = $days + $godays;
		$phase3 = ($calc_days % 33) / 33 * 2 * pi();
		$y3 = sin($phase3);

		// $str=$this->GetNormalEvenBio($week,$trainigs,$br,$been,round($y3,2) ).' <br/>'.round($y3,2).' '.round($phase3,2).' '.$calc_days;

		$str = $this->GetNormalEvenBio($week, $trainigs, $br, $been, round($y3, 2)) . ' ';
		return ' ' . $str . ' ';
		}

	public

	function Calc7($week, $trainigs, $t, $br = '<br/>')
		{
		$week++;
		$trainigs++;
		$been = ($week - 1) * $t + $trainigs;

		// $str=' '.$week.' '.$trainigs.' '.$this->freq.' '.$this->id.' ';
		// $str=' '.$been.' '.($trainigs%2);

		$str = $this->GetNormalEvenStatic($week, $trainigs, $br, $been);
		return ' ' . $str . ' ';
		}

	public

	function Calc2($week, $trainigs, $t, $br = '<br/>')
		{
		$week++;
		$trainigs++;
		$been = ($week - 1) * $t + $trainigs;

		// $str=' '.$week.' '.$trainigs.' '.$this->freq.' '.$this->id.' ';
		// $str=' '.$been.' '.($trainigs%2);

		$str = $this->GetNormalOdd($week, $trainigs, $br, $been);
		return ' ' . $str . ' ';
		}

	public

	function Calc4($week, $trainigs, $t, $br = '<br/>')
		{
		$week++;
		$trainigs++;
		$been = ($week - 1) * $t + $trainigs;

		// $str=' '.$week.' '.$trainigs.' '.$this->freq.' '.$this->id.' ';
		// $str=' '.$been.' '.($trainigs%2);

		if ($week < $this->weeks - 1)
			{
			$str = $this->GetNormalOdd($week, $trainigs, $br, $been);
			}
		elseif ($week == $this->weeks - 1)
			{
			$str = $this->GetNormalOdd100($week, $trainigs, $br, $been);
			}
		elseif ($week == $this->weeks)
			{
			$str = $this->GetNormalOdd75($week, $trainigs, $br, $been);
			}

		return ' ' . $str . ' ';
		}

	public

	function Calc6($week, $trainigs, $t, $days, $br = '<br/>')
		{
		$week++;
		$trainigs++;
		$rest = (7 - $this->trainigs) * $this->weeks;
		$been = ($week - 1) * $t + $trainigs;
		$godays = $rest + $been;
		$calc_days = $days + $godays;
		$phase3 = ($calc_days % 33) / 33 * 2 * pi();
		$y3 = 1 - sin($phase3);

		// $str=' '.$week.' '.$trainigs.' '.$this->freq.' '.$this->id.' ';
		// $str=' '.$been.' '.($trainigs%2);
		// $str=$this->GetNormalOddBio($week,$trainigs,$br,$been,$y3).' <br/>'.$y3.' '.$phase3.' '.$calc_days;

		$str = $this->GetNormalOddBio($week, $trainigs, $br, $been, $y3) . ' ';
		return ' ' . $str . ' ';
		}

	public

	function Calc8($week, $trainigs, $t, $br = '<br/>')
		{
		$week++;
		$trainigs++;
		$been = ($week - 1) * $t + $trainigs;

		// $str=' '.$week.' '.$trainigs.' '.$this->freq.' '.$this->id.' ';
		// $str=' '.$been.' '.($trainigs%2);

		$str = $this->GetNormalOddStatic($week, $trainigs, $br, $been);
		return ' ' . $str . ' ';
		}

	function Trainer($id, $name, $img, $mass, $freq, $weeks, $trainigs, $find = false)
		{
		$this->id = $id;
		$this->name = $name;
		$this->img = $img;
		$this->freq = $freq;
		$this->mass = $mass;
		$this->weeks = $weeks;
		$this->trainigs = $trainigs;
		$this->all = $this->weeks * $this->trainigs;
		$this->procents = array(
			75,
			80,
			85,
			90,
			95,
			100
		);
		$this->procents2 = array(
			80,
			85,
			90,
			100
		);
		$this->procents3 = array(
			80,
			90,
			100
		);
		$this->PrilepinTable = array(
			array(
				'left' => 55,
				'right' => 65,
				'rep' => 3,
				'sets' => 6,
				'optimal' => 24,
				'range_left' => 18,
				'range_right' => 30
			) ,
			array(
				'left' => 70,
				'right' => 80,
				'rep' => 3,
				'sets' => 6,
				'optimal' => 18,
				'range_left' => 12,
				'range_right' => 24
			) ,
			array(
				'left' => 80,
				'right' => 90,
				'rep' => 2,
				'sets' => 4,
				'optimal' => 15,
				'range_left' => 10,
				'range_right' => 20
			) ,
			array(
				'left' => 90,
				'right' => 100,
				'rep' => 1,
				'sets' => 2,
				'optimal' => 4,
				'range_left' => 10,
				'range_right' => 10
			) ,
		);

		// stumimas ir rovimas klasika

		$this->planning = array(
			75 => '4 pr. x 4 pak.',
			80 => '5 pr. x 3 pak.',
			85 => '5 pr. x 3 pak.',
			90 => '4 pr. x 2 pak.',
			95 => '4 pr. x 2 pak.',
			100 => '3 pr. x 1 pak.',
			'max' => 'maksimalus'
		);

		// trauka

		$this->planning2 = array(
			75 => '5 pr. x 5 pak.',
			80 => '5 pr. x 4 pak.',
			85 => '5 pr. x 3 pak.',
			90 => '4 pr. x 2 pak.',
			95 => '4 pr. x 2 pak.',
			100 => '3 pr. x 1 pak.',
			'max' => 'maksimalus'
		);

		// stumimas nuo krutines ir nugaros

		$this->planning3 = array(
			75 => '3 pr. x 3 pak.',
			80 => '3 pr. x 2 pak.',
			85 => '3 pr. x 2 pak.',
			90 => '2 pr. x 1 pak.',
			95 => '2 pr. x 1 pak.',
			100 => '2 pr. x 1 pak.',
			'max' => 'maksimalus'
		);

		// trauka rovimo ir stumimo

		$this->planning4 = array(
			90 => '5 pr. x 3 pak.',
			100 => '4 pr. x 4 pak.',
			105 => '4 pr. x 3 pak.',
			110 => '4 pr. x 2 pak.',
			'max' => 'maksimalus'
		);

		// pritupimai stanga ant nugaros

		$this->planning5 = array(
			75 => '5 pr. x 5 pak.',
			80 => '5 pr. x 4 pak.',
			85 => '4 pr. x 4 pak.',
			90 => '4 pr. x 3 pak.',
			95 => '5 pr. x 2 pak.',
			100 => '5 pr. x 2 pak.',
			'max' => 'maksimalus'
		);
		if ($find)
			{
			$this->const1 = 3;
			$first_proc = 0.5;
			$second_proc = 0.3;
			$third_proc = 1 - $second_proc - $first_proc;
			$first = round($first_proc * $this->all);
			$second = round($second_proc * $this->all);
			$third = $this->all - $first - $second;
			$mas = array();
			$mas1 = array();
			$mas2 = array();
			$mas3 = array();
			$ccount = 0;
			$ccountt = 0;
			$ccountt2 = 0;
			for ($i = 1; $i <= $this->all; $i++)
				{

				// echo "i=".$i."\n";

				$rnd = $this->random_int_float(0 + $ccountt2, count($this->procents) - 1 - $ccount);
				$p = $this->procents[$rnd];
				if ($p == 100)
					{
					$ccount = 1;
					}
				elseif ($p == 75)
					{
					$ccountt++;
					}

				if ($ccountt > 2)
					{
					$ccountt2 = 1;
					}

				$mas[$i] = array(
					'i' => $i,
					'type' => 'a',
					'procents' => $p,
					'planning' => $this->planning[$p],
					'change' => 1
				);
				}

			$this->const2 = 3;
			$j = $this->const2;
			while ($j < $this->all)
				{
				$mas[$j]['type'] = 'b';
				$j = $j + $this->const2;
				}

			$this->const3 = 5;
			$j = $this->const3;
			while ($j < $this->all)
				{
				$mas[$j]['type'] = 'c';
				$j = $j + $this->const3;
				}

			$ccount2 = 0;
			$ccountr = 0;
			$ccountr2 = 0;
			$ccount3 = 0;
			$ccountp = 0;
			$ccountp2 = 0;
			foreach($mas as $key => $value)
				{
				if (strcmp($value['type'], 'a') === 0)
					{
					$mas1[] = $key;
					$mas[$key]['planning'] = $this->planning[$value['procents']];
					$mas[$key]['change'] = 2;
					}
				elseif (strcmp($value['type'], 'b') === 0)
					{
					$rnd = $this->random_int_float(0 + $ccountr2, count($this->procents2) - 1 - $ccount2);
					$p = $this->procents[$rnd];
					if ($p == 100)
						{
						$ccount2 = 1;
						}
					elseif ($p == 75)
						{
						$ccountr++;
						}

					if ($ccountr > 1)
						{
						$ccountr2 = 1;
						}

					$mas[$key]['procents'] = $p;
					$mas[$key]['planning'] = $this->planning2[$p];
					$mas[$key]['change'] = 2;
					$mas2[] = $key;
					}
				elseif (strcmp($value['type'], 'c') === 0)
					{
					$rnd = $this->random_int_float(0 + $ccountp2, count($this->procents3) - 1 - $ccount3);
					$p = $this->procents[$rnd];
					if ($p == 100)
						{
						$ccount3 = 1;
						}
					elseif ($p == 75)
						{
						$ccountp++;
						}

					if ($ccountp > 1)
						{
						$ccountr3 = 1;
						}

					$mas[$key]['procents'] = $p;
					$mas[$key]['planning'] = $this->planning3[$p];
					$mas[$key]['change'] = 2;
					$mas3[] = $key;
					}
				}

			$this->mas = $mas;
			$this->mas1 = $mas1;
			$this->mas2 = $mas2;
			$this->mas3 = $mas3;
			}
		  else
			{
			$this->const1 = null;
			$this->const2 = null;
			$this->const3 = null;
			}
		}
	}

?>