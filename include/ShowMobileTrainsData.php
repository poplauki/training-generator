<?php
$answer = array();
$answer['answer'] = false;

if (isset($snatch) && isset($cleanjerk) && isset($dvik) && isset($standfront) && isset($standbehind) && isset($squats_front) && isset($squats_back) && isset($weeks) && isset($tranings))
	{
	$mas = array();
	if (class_exists('Trainer'))
		{
		$mas['snatch'] = new Trainer(1, 'Rovimas klasika', 'snatch.png', $snatch, 1, $weeks, $tranings, true);
		$mas['snatch_high'] = new Trainer(2, 'Rovimas nuo pakibimo', 'snatch_high.png', $snatch, 1, $weeks, $tranings, true);
		$mas['snatch_strenght'] = new Trainer(3, 'Trauka rovimo', 'snatch_strenght.png', $snatch, 2, $weeks, $tranings, false);
		$mas['cleanjerk'] = new Trainer(4, 'Stumimas klasika', 'cleanjerk.png', $cleanjerk, 2, $weeks, $tranings, true);
		$mas['cleanjerk_high'] = new Trainer(5, 'Stumimas nuo pakibimo', 'cleanjerk_high.png', $cleanjerk, 2, $weeks, $tranings, true);
		$mas['clean'] = new Trainer(6, 'Štangos paėmimas ant krutinės', 'clean.png', $cleanjerk, 2, $weeks, $tranings, true);
		$mas['cleanjerk_strenght'] = new Trainer(7, 'Trauka stumimo', 'cleanjerk_strenght.png', $cleanjerk, 1, $weeks, $tranings, false);
		$mas['standfront'] = new Trainer(8, 'Štangos stumimas nuo krutinės (nuo stovu)', 'standfront.png', $standfront, 1, $weeks, $tranings, false);
		$mas['standbehind'] = new Trainer(9, 'Štangos stumimas nuo nugaros (nuo stovu)', 'standbehind.png', $standbehind, 2, $weeks, $tranings, false);
		$mas['squats_front'] = new Trainer(10, 'Pritupimai štanga ant krutinės', 'squats_front.png', $squats_front, 2, $weeks, $tranings, false);
		$mas['squats_back'] = new Trainer(11, 'Pritupimai štanga ant nugaros', 'squats_back.png', $squats_back, 1, $weeks, $tranings, false);
		}

	$answer['answer'] = true;

	// <img src="images/pdf.png" width="24" height="24" border="0" id="generatePDF" alt="weeks='.$weeks.'&tranings='.$tranings.'&squats_back='.$squats_back.'&squats_front='.$squats_front.'&standbehind='.$standbehind.'&standfront='.$standfront.'&cleanjerk='.$cleanjerk.'&snatch='.$snatch.'&dvik='.$dvik.'"> - atsiųsti treneruočiu lentelė PDF bylą<br/>
	// <img src="images/pdf.png" width="24" height="24" border="0" id="generatePDFList" alt="weeks='.$weeks.'&tranings='.$tranings.'&squats_back='.$squats_back.'&squats_front='.$squats_front.'&standbehind='.$standbehind.'&standfront='.$standfront.'&cleanjerk='.$cleanjerk.'&snatch='.$snatch.'&dvik='.$dvik.'"> - atsiųsti treneruočiu sąrašą PDF bylą<br/>

	$mtable = '<a href="#documentation">Sugeneruotos treneruočių programos paaiškinimas</a><br/>
				<img src="images/doc.png" width="24" height="24" border="0" id="generatePrint" alt="weeks=' . $weeks . '&tranings=' . $tranings . '&squats_back=' . $squats_back . '&squats_front=' . $squats_front . '&standbehind=' . $standbehind . '&standfront=' . $standfront . '&cleanjerk=' . $cleanjerk . '&snatch=' . $snatch . '&dvik=' . $dvik . '&dvik=' . $dvik . '"> - lentelės spausdinimo versija (atsidaro naujam lange, po atsidarymo spaudžiame klavišų kombinaciją CTRL + P, kurį iškviečia spausdintuvo pasirinkimą)<br/>
		<img src="images/doc.png" width="24" height="24" border="0" id="generatePrintList" alt="weeks=' . $weeks . '&tranings=' . $tranings . '&squats_back=' . $squats_back . '&squats_front=' . $squats_front . '&standbehind=' . $standbehind . '&standfront=' . $standfront . '&cleanjerk=' . $cleanjerk . '&snatch=' . $snatch . '&dvik=' . $dvik . '&dvik=' . $dvik . '"> - sąrašo spausdinimo versija (atsidaro naujam lange, po atsidarymo spaudžiame klavišų kombinaciją CTRL + P, kurį iškviečia spausdintuvo pasirinkimą)<br/>

';
	$mtable.= '';
	$all = $weeks * $tranings;
	$t = 0;
	if (isset($oddeven) && $oddeven)
		{
		for ($i = 0; $i < $weeks; $i++)
			{
			for ($j = 0; $j < $tranings; $j++)
				{
				$t++;
				$mtable.= ' <div class="training" border="1" style="border:solid thin black;padding:0;margin:2px 0 2px 0;">Treniruotė nr. ' . $t . '  (' . ($i + 1) . ' savaitė ' . Roman_Number($j + 1) . ' treniruotė) <br/> ';
				$source_in = ' ';
				if (!empty($mas))
					{
					foreach($mas as $key => $value)
						{
						switch ($type_calculate)
							{
						case 'linear':
							$source_in_mas = ' ' . $value->Calc2($i, $j, $tranings) . ' ';
							break;

						case 'competition':
							$source_in_mas = ' ' . $value->Calc4($i, $j, $tranings) . ' ';
							break;

						case 'biorythm':
							$source_in_mas = ' ' . $value->Calc6($i, $j, $tranings, $birthdate['days']) . ' ';
							break;

						case 'static':
							$source_in_mas = ' ' . $value->Calc8($i, $j, $tranings) . ' ';
							break;
							}

						if (trim($source_in_mas) != '')
							{
							$source_in.= '<p><span class="name">' . $value->getNameMin() . ' ' . $value->name . '</span> ' . $source_in_mas . '</p> ';
							}
						}
					}

				$source_in.= ' ';
				$mtable.= $source_in . ' </div> ';
				}

			$mtable.= '<hr>';
			}
		}
	  else
		{
		for ($i = 0; $i < $weeks; $i++)
			{
			for ($j = 0; $j < $tranings; $j++)
				{
				$t++;
				$mtable.= ' <div class="training" border="1" style="border:solid thin black;padding:0;margin:2px 0 2px 0;">Treniruotė nr. ' . $t . '  (' . ($i + 1) . ' savaitė ' . Roman_Number($j + 1) . ' treniruotė) <br/> ';
				$source_in = ' ';
				if (!empty($mas))
					{
					foreach($mas as $key => $value)
						{
						switch ($type_calculate)
							{
						case 'linear':
							$source_in_mas = ' ' . $value->Calc($i, $j, $tranings) . ' ';
							break;

						case 'competition':
							$source_in_mas = ' ' . $value->Calc3($i, $j, $tranings) . ' ';
							break;

						case 'biorythm':
							$source_in_mas = ' ' . $value->Calc5($i, $j, $tranings, $birthdate['days']) . ' ';
							break;

						case 'static':
							$source_in_mas = ' ' . $value->Calc7($i, $j, $tranings) . ' ';
							break;
							}

						if (trim($source_in_mas) != '')
							{
							$source_in.= '<p><span class="name">' . $value->getNameMin() . ' ' . $value->name . '</span> ' . $source_in_mas . '</p> ';
							}
						}
					}

				$source_in.= ' ';
				$mtable.= $source_in . ' </div> ';
				}

			$mtable.= '<hr>';
			}
		}

	$help = '';
	if (!empty($mas))
		{
		$help.= '<br/><div><a name="documentation">Aprašymas</a>:
Pratimų atvaizdai  :<br/>';
		$help.= '<div>';
		$i = 3;
		foreach($mas as $key => $value)
			{
			$help.= ' ' . $value->GetLink() . ' - ' . $value->getName() . ' <br/>';
			$i++;
			}

		$help.= '</div>';
		$help.= 'Treniruotės išdėstymas yra bloko tipo, t. y. kekviena treniruote įrašyta į atskira bloką kur išvardyti visi pratimai ir yra apskaičiuota kiek procentu arba kilogramų reikia pakelti, kiek padaryti priėjimų ir pakartojimų.</div>';
		}

	$answer['data'] = $mtable;
	$answer['help'] = $help;
	}

echo '<div id="contentblock">' . $answer['data'] . '</div><div id="helpblock">' . $answer['help'] . '</div>';
?>